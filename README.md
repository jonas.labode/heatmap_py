# 3D heatmap plot
This program takes a .csv of 3d coordinates and plots them with a density color coding using matplotlib.
The functionality is limited to the absolute basics. The user is invited to use it as a starting point and adapt it to their liking.


## Requirements
Required packages are listed in requirements.txt and can be installed using pip as follows:\
`pip3 install -r requirements.txt`

## Input
- .csv file containing the coordinates; A header is expected, thus the first line of the file is excluded. The expected format of the body is: `label (will not be used in any way), x_coordinate, y_coordinate, z_coordinate`

## Output
- matplotlib will display the plot in an interactive window

## Usage example
`python3 heatmap.py myfile.csv`