import argparse
import csv
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt


def read_points(file_name):
    point_x = []
    point_y = []
    point_z = []

    with open(file_name) as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        next(reader, None)  # skip the header
        for row in reader:
            # We expect the format id, x, y, z
            # The id column is skipped, as we do not need it for our analysis
            point_x.append(int(row[1]))
            point_y.append(int(row[2]))
            point_z.append(int(row[3]))

    return [point_x, point_y, point_z]


def calculate_density(points):
    xyz = np.vstack(points)
    kde = stats.gaussian_kde(xyz)
    density = kde(xyz)

    return density


def plot(points, density):
    fig = plt.figure(figsize=(12, 12))
    ax = fig.add_subplot(projection='3d')
    ax.scatter(points[0], points[1], points[2], c=density, alpha=0.05)
    plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Plot 3d coordinates with a heatmap (density) color coding')
    parser.add_argument('csv_file', action='store', type=str, help='.csv file containing id/coordinates list')

    args = parser.parse_args()

    csv_file_path = args.csv_file

    coordinates = read_points(csv_file_path)
    density_data = calculate_density(coordinates)
    plot(coordinates, density_data)

